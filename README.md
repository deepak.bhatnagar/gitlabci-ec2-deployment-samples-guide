# Gitlab-CI Deployment to AWS EC2 Using S3, CodePipeline and CodeDeploy

## Introduction
Sample scripts and guide intended as a template for deploying an application from Gitlab to AWS EC2 using AWS CodePipeline, CodeDeploy and S3

## Scope
This collection of sample scripts assumes the following
 
 - Gitlab EE hosted project - may work on private EE instances (not tested)
 - Gitlab as the GIT versioning repository
 - Gitlab-CI as the Continuous Integration Engine
 - Existing AWS account
 - AWS EC2 as the target production or staging system for the deployment
 - AWS EC2 Instance running Amazon Linux AMI
 - AWS S3 as the storage facility for deployment files
 - AWS CodeDeploy as the Deployment engine for the project
 - AWS CodePipeline as the Pipeline for deployment

The provided `.gitlab-ci.yml` sample is based on a Java/Scala + Gradle project.
The script is provided as a generic example and will need to be adapted to your specific needs when implementing Continuous Delivery through this method.

The guide will assume that the user has basic knowledge about AWS services and how to perform the necessary tasks.

**Note**: The guide provided in this sample uses the AWS console to perform tasks. While there are likely CLI equivalent for the tasks performed here, these will not be covered throughout the guide. 

## Motivation
The motivation for creating these scripts and deployment guide came from the lack of availability of a proper tutorial showing how to implement Continuous Delivery using Gitlab and AWS EC2.
Gitlab introduced their freely available CI engine by partnering with Digital Ocean, which enables user repositories to benefit from good quality CI for free.

One of the main advantages of using Gitlab is that they provide built-in Continuous Integration containers for running through the various steps and validate a build.
Unfortunately, Gitblab nor AWS provide an integration that would allow to perform Continuous Deliver following passing builds.

This Guide and Scripts provide a simplified version of the steps that I've undertaken in order to have a successful CI and CD using both Gitlab and AWS EC2.

## What is provided
- sample `gitlab-ci.yml` script - based on a Gradle project
- sample `appspec.yml` AWS CodeDeploy sample script
- sample AWS CodeDeploy hook scripts - referenced by `appspec.yml`
- sample runner script - `my_app.sh`
- sample distribution zip file - contains only structure required by CodeDeploy - based on provided samples

## Guide
see GUIDE.md for full Guide---